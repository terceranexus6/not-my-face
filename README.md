# Not My Face

Stuff I use to fight against facial recognition.
The amazing [python library](https://github.com/ageitgey/face_recognition) I'm using is made by [Adam Geitgey](https://github.com/ageitgey) and the data is taken is from public sources of famous people, made by [Davis King](https://github.com/davisking). Thank you so much for publishing such a comprehensive library.

Current ways I'm trying:

- Transparent mask 3D printed
- IR Lights
- Makeup
- Laser
